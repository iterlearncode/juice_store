<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt,
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'db_juice_store' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Z22nbfXOh!Qwn$)i_F_E -,0riOGgt%cB!mHe a|oY!3P:Qytb(j{xg,|A-z`W{?' );
define( 'SECURE_AUTH_KEY',  '-S#Y)/dr<9Dbfxd+#nE(~)53m=[F6cR~`  Z]zWH?*}k|,[m)%SGpUQ#X.Fl(X{;' );
define( 'LOGGED_IN_KEY',    'sFT99[EP0wH%;&K+97>l.O-:wu%dQsNfy$ifJiz^wV|T>D{8O?C:~pYf4~QC_[e0' );
define( 'NONCE_KEY',        'QUf&:Js0;S$td!TT^5Ha70~GxpXDZs[,z}5O:XQLv|o7}f!5Ft{!9e8k-l%!kmxZ' );
define( 'AUTH_SALT',        ':BZfPWeY^rY{,u qQ 5E>31uW~jj|eD+Os/h]&z40%% *]{}O =4%D5hNfXd{/%C' );
define( 'SECURE_AUTH_SALT', '{*=@b/LJP1p*rw4<A6jX9d[V1^LXDOfpzR=JuZ7V2.{v%_tEC5QIA;7.BL;8pSVG' );
define( 'LOGGED_IN_SALT',   '|jb]FK/(%+Fl{kY*r.24>13RG+UZ@-QBT  #Q#/7%WE<[Vv9=+(Y8s>ZL9hnY87`' );
define( 'NONCE_SALT',       'X6zS2n32Iw|0~[Futj&JzS%pdwj/.y]Z]vDGpU(v0I]h%2i*D^R)L2e%GTJ;C-&y' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
